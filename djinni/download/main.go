package main

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats/djinni"
	"io/ioutil"
	"os"
)

func main() {
	var cachePath = os.Getenv("CACHE_PATH")

	if cachePath == "" {
		panic("require environment")
	}

	for i := 1; i <= djinni.VacancyPageCount; i++ {
		func() {
			var (
				url       = fmt.Sprintf("https://djinni.co/jobs/?page=%d", i)
				cachePath = fmt.Sprintf("%s/%d", cachePath, i)
			)

			var (
				request  = fasthttp.AcquireRequest()
				response = fasthttp.AcquireResponse()
			)

			defer func() {
				fasthttp.ReleaseRequest(request)
				fasthttp.ReleaseResponse(response)
			}()

			request.SetRequestURI(url)

			var err = fasthttp.DoRedirects(request, response, 1)
			if err != nil {
				fmt.Printf("error on fetch %s: %+v", url, err)

				return
			}

			err = ioutil.WriteFile(cachePath, response.Body(), 0777)
			if err != nil {
				fmt.Printf("error on cache %s: %+v", url, err)

				return
			}
		}()
	}
}
