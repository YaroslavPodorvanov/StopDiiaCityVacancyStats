package main

import (
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats/djinni"
	"gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats/more"
	verify "gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats/verify/djinni"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	var cachePath = os.Getenv("CACHE_PATH")

	if cachePath == "" {
		panic("require environment")
	}

	var (
		totalCount        = 0
		stopDiiaCityCount = 0
		companyMap        = make(map[string]int)
	)

	for i := 1; i <= djinni.VacancyPageCount; i++ {
		var (
			cachePath = fmt.Sprintf("%s/%d", cachePath, i)
		)

		var content, readErr = ioutil.ReadFile(cachePath)
		if readErr != nil {
			panic(readErr)
		}

		var document, parseErr = goquery.NewDocumentFromReader(bytes.NewReader(content))
		if parseErr != nil {
			panic(parseErr)
		}

		document.Find("div.list-jobs__details__info a").Each(func(_ int, selection *goquery.Selection) {
			var path, ok = selection.Attr("href")

			if ok && strings.HasPrefix(path, "/jobs/") {
				totalCount += 1

				var url = "https://djinni.co" + path
				if verify.StopDiiaCity(url) {
					stopDiiaCityCount += 1
				} else {
					companyMap[url] += 1
				}
			}
		})
	}

	fmt.Printf("Djinni total: %5d, stopdiiacity: %5d\n", totalCount, stopDiiaCityCount)

	more.More(companyMap)
}
