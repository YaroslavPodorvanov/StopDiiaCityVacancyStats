#
-include Makefile.dev

run:
	mkdir -p ./cache

	VACANCY_STORAGE_PATH=$(PWD)/cache/vacancy.proto \
	DOU_CSFR_TOKEN=$(DOU_CSFR_TOKEN) \
	DOU_CSFR_MIDDLEWARE_TOKEN=$(DOU_CSFR_MIDDLEWARE_TOKEN) \
	go run main.go

djinni-download:
	mkdir -p ./djinni/cache

	CACHE_PATH=$(PWD)/djinni/cache \
	time go run ./djinni/download/main.go

djinni-parse:
	CACHE_PATH=$(PWD)/djinni/cache \
	time go run ./djinni/parse/main.go