package more

import (
	"encoding/json"
	"fmt"
	"sort"
)

type Company struct {
	URL   string `json:"url"`
	Count int    `json:"count"`
}

func More(source map[string]int) {
	var companies = make([]Company, 0, len(source))

	for url, count := range source {
		companies = append(companies, Company{
			URL:   url,
			Count: count,
		})
	}

	sort.Slice(companies, func(i, j int) bool {
		return companies[i].Count > companies[j].Count
	})

	var content, err = json.Marshal(companies)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(content))
}
