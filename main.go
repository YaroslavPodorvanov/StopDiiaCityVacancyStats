package main

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats/more"
	verify "gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats/verify/dou"
	"gitlab.com/qrsx/dou-scrapper/components/dou"
	. "gitlab.com/qrsx/dou-scrapper/models/dou"
	"math"
	"os"
	"strings"
)

func main() {
	var (
		path                    = os.Getenv("VACANCY_STORAGE_PATH")
		cookieCSFRToken         = os.Getenv("DOU_CSFR_TOKEN")
		bodyCSFRMiddlewareToken = os.Getenv("DOU_CSFR_MIDDLEWARE_TOKEN")
	)

	if path == "" || cookieCSFRToken == "" || bodyCSFRMiddlewareToken == "" {
		panic("require environment")
	}

	var (
		storage = dou.NewVacancyStorage(path, math.MaxUint32)
		client  = dou.NewClient(new(fasthttp.Client), Credentials{
			UserAgent:               "",
			CookieCSFRToken:         cookieCSFRToken,
			BodyCSFRMiddlewareToken: bodyCSFRMiddlewareToken,
		})
		scrapper = dou.NewVacancyScrapper(storage, client)

		companyMap = make(map[string]int)
	)

	var vacancies, _, err = scrapper.DiffScrap()
	if err != nil {
		panic(err)
	}

	var (
		stopDiiaCityCount = 0
	)

	for _, vacancy := range vacancies {
		if verify.StopDiiaCity(vacancy.Url) {
			stopDiiaCityCount += 1
		} else {
			companyMap[extractCompanyURL(vacancy.Url)] += 1
		}
	}

	fmt.Printf("DOU total: %5d, stopdiiacity: %5d\n", len(vacancies), stopDiiaCityCount)

	more.More(companyMap)
}

func extractCompanyURL(fullUrl string) string {
	startIndex := strings.Index(fullUrl, "/companies/")

	rest := fullUrl[startIndex+len("/companies/"):]

	endIndex := strings.Index(rest, "/")

	return fullUrl[:startIndex+len("/companies/")+endIndex]
}
