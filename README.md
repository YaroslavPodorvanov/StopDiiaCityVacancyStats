# StopDiiaCity vacancy stats

### [DOU](https://jobs.dou.ua/vacancies/)
* 1308 from 9420 vacancies is StopDiiaCity (14%)
* 1433 from 8941 vacancies is StopDiiaCity (16%)
* 1643 from 9406 vacancies is StopDiiaCity (17%)

### [Djinni](https://djinni.co/jobs/)
* 4364 from 25980 vacancies is StopDiiaCity (17%)
* 7031 from 28980 vacancies is StopDiiaCity (24%)
* 7100 from 28980 vacancies is StopDiiaCity (25%)
