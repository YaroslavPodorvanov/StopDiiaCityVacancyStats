package verify

import "strings"

func StopDiiaCity(prefixes []string, url string) bool {
	return hasAnyPrefix(prefixes, url)
}

func hasAnyPrefix(prefixes []string, url string) bool {
	for _, prefix := range prefixes {
		if strings.HasPrefix(url, prefix) {
			return true
		}
	}

	return false
}
