package djinni

// source https://gitlab.com/YaroslavPodorvanov/StopDiiaCity/-/blob/main/verify/verify.go
var companies = []string{
	"https://djinni.co/jobs/company-allright-cc765/",
	"https://djinni.co/jobs/company-englishdom-209e8/",
	"https://djinni.co/jobs/company-powercode-9f88a/",
	"https://djinni.co/jobs/company-genesis-bbc83/",                   // genesis
	"https://djinni.co/jobs/company-gen-tech-f1f4f/",                  // genesis
	"https://djinni.co/jobs/company-gen-tech2-427ee/",                 // genesis
	"https://djinni.co/jobs/company-genesis-tech-b88e2/",              // genesis
	"https://djinni.co/jobs/company-amomedia-4c317/",                  // genesis
	"https://djinni.co/jobs/company-headway-app-81bee/",               // genesis
	"https://djinni.co/jobs/company-solid-fintech-company-87d5d/",     // genesis
	"https://djinni.co/jobs/company-nebula-project-by-genesis-ed113/", // genesis
	"https://djinni.co/jobs/company-socialtech-6b80f/",                // genesis
	"https://djinni.co/jobs/company-genesis-holywater--6c47c/",        // genesis
	"https://djinni.co/jobs/company-genesis-apps-fintech-58625/",      // genesis
	"https://djinni.co/jobs/company-dc-de2ed/",                        // genesis
	"https://djinni.co/jobs/company-betterme-4372c/",                  // genesis
	"https://djinni.co/jobs/company-redtrack-io-3b68b/",               // genesis
	"https://djinni.co/jobs/?keywords=Genesis",                        // genesis
	"https://djinni.co/jobs/company-jooble-e95dd/",
	"https://djinni.co/jobs/company-netpeak-group-20216/",
	"https://djinni.co/jobs/company-saldo-apps-e0cc6/", // netpeak
	"https://djinni.co/jobs/company-leverx-com-47815/",
	"https://djinni.co/jobs/company-refaceai-7538f/",
	"https://djinni.co/jobs/company-s-pro-4cd02/",
	"https://djinni.co/jobs/company-banza-f738b/",
	"https://djinni.co/jobs/company-datagroup-a26a2/",
	"https://djinni.co/jobs/company-deusrobots-com-46e96/",
	"https://djinni.co/jobs/company-parimatch-tech-b6a34/",
	"https://djinni.co/jobs/company-parimatch-international-1b06c/",
	"https://djinni.co/jobs/company-pokermatch-com-43742/",
	"https://djinni.co/jobs/company-epam-systems-bb0df/",
	"https://djinni.co/jobs/company-ajax-systems-8b02d/",
	"https://djinni.co/jobs/company-kyivstar-c5f1a/",
	"https://djinni.co/jobs/company-nika-tech-family-d3d98/", // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://djinni.co/jobs/company-sigma-software-c03a7/",
	"https://djinni.co/jobs/company-ideasoft-dbe69/",
	"https://djinni.co/jobs/company-ideasoft-io-40c97/",
	"https://djinni.co/jobs/company-intetics-5221d/",
	"https://djinni.co/jobs/company-intetics-minsk-cb2dc/",
	"https://djinni.co/jobs/company-playson-05bc8/",
	"https://djinni.co/jobs/company-smartiway-6d23c/",
	"https://djinni.co/jobs/company-ciklum-international-80662/",
	"https://djinni.co/jobs/company-nix-solutions-fe08e/",
	"https://djinni.co/jobs/company-softserve-6bee7/",
	"https://djinni.co/jobs/company-softserve-dnipro-58f42/",
	"https://djinni.co/jobs/company-softserve-lviv-0de17/",
	"https://djinni.co/jobs/company-softserve-kharkiv-9b88e/",
	"https://djinni.co/jobs/company-raiffeisen-bank-aval-7b988/",
	"https://djinni.co/jobs/company-raiffeisen-bank-international-4cb67/",
	"https://djinni.co/jobs/company-eleks-6227d/",
	"https://djinni.co/jobs/company-petcube-com-a1c55/",
	"https://djinni.co/jobs/company-softteco-c9269/",
	"https://djinni.co/jobs/company-luxoft-ec4fe/",
	"https://djinni.co/jobs/company-adtelligent-751ce/",
	"https://djinni.co/jobs/company-intersog-fa680/",
	"https://djinni.co/jobs/company-gismart-e0be5/",
	"https://djinni.co/jobs/company-softpositive-37a51/",
	"https://djinni.co/jobs/company-vodafone-ukraine-85a78/",
	"https://djinni.co/jobs/company-plvision-a0d4d/",
	"https://djinni.co/jobs/company-treeum-6d9c0/",
	"https://djinni.co/jobs/company-globallogic-43eee/",
	"https://djinni.co/jobs/company-graintrack-cd726/",
	"https://djinni.co/jobs/company-intellias-c99a7/",        // question
	"https://djinni.co/jobs/company-astound-commerce-a1b13/", // https://dou.ua/lenta/articles/what-it-companies-think-about-bill-5376/
	"https://djinni.co/jobs/company-wargaming-325df/",
	"https://djinni.co/jobs/company-glovo-a9cf1/",                   // Glovo
	"https://djinni.co/jobs/company-don-t-panic-recruitment-4f656/", // Glovo proxy https://web.archive.org/web/20211111175135/https://djinni.co/jobs/289484-frontend-engineer-v-glovo/
	"https://djinni.co/jobs/company-staffingpartner-162e0/",
	"https://djinni.co/jobs/company-smart-solutions-33094/", // https://jobs.dou.ua/companies/smart-solutions/
	"https://djinni.co/jobs/company-fintech-band-3a8e1/",
	"https://djinni.co/jobs/company-fintech-farm-93fc9/",
	"https://djinni.co/jobs/company-itransition-6d0d7/", // https://dou.ua/forums/topic/35889/

	"https://djinni.co/jobs/company-sendpulse-d0078/",     // https://dou.ua/lenta/articles/companies-about-diia-city/
	"https://djinni.co/jobs/company-smart-project-9debc/", // https://dou.ua/lenta/articles/companies-about-diia-city/
	"https://djinni.co/jobs/company-zagrava-70810/",       // https://dou.ua/lenta/articles/companies-about-diia-city/

	"https://djinni.co/jobs/company-heyman-ai-08229/",  // revolut https://dou.ua/lenta/news/diia-city-has-been-officially-launched/
	"https://djinni.co/jobs/company-macpaw-76eae/",     // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/
	"https://djinni.co/jobs/company-govitall-f73ad/",   // calmerry https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://djinni.co/jobs/company-innovecs-2a027/",   // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://djinni.co/jobs/company-ilogos-c864c/",     // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://djinni.co/jobs/company-aiesec-net-e5eff/", // natus-vincere https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469

	"https://djinni.co/jobs/company-megogo-b5ed6/", // https://dev.ua/news/megogo-vstupaeie-v-diia-sity

	"https://djinni.co/jobs/company-roosh-recruitment-eac0d/", // https://dou.ua/lenta/news/44-companies-applied-to-join-diia-city/
	"https://djinni.co/jobs/company-trios-systems-56c37/",     // https://dou.ua/lenta/interviews/nuzhnyi-about-trios/
	"https://djinni.co/jobs/company-softblues-df7bb/",         // https://dou.ua/lenta/interviews/nuzhnyi-about-trios/

	"https://djinni.co/jobs/company-samsung-r-d-institute-ukraine-2c782/",       // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://djinni.co/jobs/company-samsung-electronics-ukraine-company-a2658/", // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://djinni.co/jobs/company-ria-3b2bd/",                                 // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://djinni.co/jobs/company-rozetka-54c48/",                             // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://djinni.co/jobs/company-plarium-kharkiv-c465f/",                     // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://djinni.co/jobs/company-plarium-kyiv-6e4d7/",                        // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://djinni.co/jobs/company-infoplius-b9038/",                           // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
}
