package dou

// source https://gitlab.com/YaroslavPodorvanov/StopDiiaCity/-/blob/main/verify/verify.go
var companies = []string{
	"https://jobs.dou.ua/companies/allright/",
	"https://jobs.dou.ua/companies/englishdom/",
	"https://jobs.dou.ua/companies/powercodelab/",
	"https://jobs.dou.ua/companies/powercode-academy/",
	"https://jobs.dou.ua/companies/genesis-technology-partners/", // Genesis
	"https://jobs.dou.ua/companies/solid/",                       // Genesis
	"https://jobs.dou.ua/companies/headway-1/",                   // Genesis
	"https://jobs.dou.ua/companies/socialtech/",                  // Genesis
	"https://jobs.dou.ua/companies/obrio/",                       // Genesis
	"https://jobs.dou.ua/companies/boosters-product-team/",       // Genesis
	"https://jobs.dou.ua/companies/betterme/",                    // Genesis
	"https://jobs.dou.ua/companies/lift-stories-editor/",         // Genesis
	"https://jobs.dou.ua/companies/sendios/",                     // Genesis
	"https://jobs.dou.ua/companies/jooble/",
	"https://jobs.dou.ua/companies/netpeak/",             // netpeak
	"https://jobs.dou.ua/companies/netpeak-group/",       // netpeak
	"https://jobs.dou.ua/companies/ringostat/",           // netpeak
	"https://jobs.dou.ua/companies/serpstat/",            // netpeak
	"https://jobs.dou.ua/companies/saldo-apps/",          // netpeak
	"https://jobs.dou.ua/companies/academyocean/",        // netpeak
	"https://jobs.dou.ua/companies/tonti-laguna/",        // netpeak
	"https://jobs.dou.ua/companies/tonti-laguna-mobile/", // netpeak
	"https://jobs.dou.ua/companies/inweb/",               // netpeak
	"https://jobs.dou.ua/companies/leverx-group/",
	"https://jobs.dou.ua/companies/reface/",
	"https://jobs.dou.ua/companies/s-pro/",
	"https://jobs.dou.ua/companies/banza/",
	"https://jobs.dou.ua/companies/datagroup/",
	"https://jobs.dou.ua/companies/deus-robots/",
	"https://jobs.dou.ua/companies/parimatch-tech/",
	"https://jobs.dou.ua/companies/pm-international/",
	"https://jobs.dou.ua/companies/pokermatch/",
	"https://jobs.dou.ua/companies/epam-systems/",
	"https://jobs.dou.ua/companies/epam-anywhere/",
	"https://jobs.dou.ua/companies/ajax-systems/",
	"https://jobs.dou.ua/companies/kyivstar/",
	"https://jobs.dou.ua/companies/nika-tech-family/", // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://jobs.dou.ua/companies/sigma-software/",
	"https://jobs.dou.ua/companies/sigma-technology-systems/",
	"https://jobs.dou.ua/companies/ideasoft-io/",
	"https://jobs.dou.ua/companies/intetics-co/",
	"https://jobs.dou.ua/companies/playson/",
	"https://jobs.dou.ua/companies/smartiway/",
	"https://jobs.dou.ua/companies/ciklum/",
	"https://jobs.dou.ua/companies/nix-solutions-ltd/",
	"https://jobs.dou.ua/companies/softserve/",
	"https://jobs.dou.ua/companies/raiffeisen/",
	"https://jobs.dou.ua/companies/raiffeisen-bank-international-ag/",
	"https://jobs.dou.ua/companies/eleks/",
	"https://jobs.dou.ua/companies/petcube-inc/",
	"https://jobs.dou.ua/companies/softteco/",
	"https://jobs.dou.ua/companies/luxoft/",          // https://dou.ua/lenta/articles/industry-about-diia-city/
	"https://jobs.dou.ua/companies/luxoft-training/", // https://dou.ua/lenta/articles/industry-about-diia-city/
	"https://jobs.dou.ua/companies/adtelligent/",
	"https://jobs.dou.ua/companies/intersog/",
	"https://jobs.dou.ua/companies/gismart_com/",
	"https://jobs.dou.ua/companies/softpositive/",
	"https://jobs.dou.ua/companies/vodafone-ukraine/",
	"https://jobs.dou.ua/companies/plvision/",
	"https://jobs.dou.ua/companies/treeum/",
	"https://jobs.dou.ua/companies/globallogic/",
	"https://jobs.dou.ua/companies/graintrack/",                   // inside
	"https://jobs.dou.ua/companies/intellias/",                    // question
	"https://jobs.dou.ua/companies/astound/",                      // https://dou.ua/lenta/articles/what-it-companies-think-about-bill-5376/
	"https://jobs.dou.ua/companies/wargaming/",                    // https://dou.ua/lenta/interviews/shumygora-about-wargaming/
	"https://jobs.dou.ua/companies/glovo/",                        // https://dou.ua/forums/topic/35378/
	"https://jobs.dou.ua/companies/dont-panic-recruiting-agency/", // agency, Glovo proxy https://web.archive.org/web/20211111175135/https://djinni.co/jobs/289484-frontend-engineer-v-glovo/
	"https://jobs.dou.ua/companies/staffingpartner/",              // agency, sigma & global proxy
	"https://jobs.dou.ua/companies/smart-solutions/",              // https://jobs.dou.ua/companies/smart-solutions/
	"https://jobs.dou.ua/companies/fintech-band/",                 // https://dou.ua/forums/topic/35880/
	"https://jobs.dou.ua/companies/fintech-farm/",                 // https://dou.ua/forums/topic/35880/
	"https://jobs.dou.ua/companies/itransition/",                  // https://dou.ua/forums/topic/35889/

	"https://jobs.dou.ua/companies/sendpulse/",                // https://dou.ua/lenta/articles/companies-about-diia-city/
	"https://jobs.dou.ua/companies/smart-project-gmbh/",       // https://dou.ua/lenta/articles/companies-about-diia-city/
	"https://jobs.dou.ua/companies/smart-project-llc/",        // https://dou.ua/lenta/articles/companies-about-diia-city/
	"https://jobs.dou.ua/companies/zagrava-games-by-playrix/", // https://dou.ua/lenta/articles/companies-about-diia-city/

	"https://jobs.dou.ua/companies/revolut/",       // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/
	"https://jobs.dou.ua/companies/macpaw/",        // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/
	"https://jobs.dou.ua/companies/calmerry/",      // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://jobs.dou.ua/companies/govitall/",      // calmerry https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://jobs.dou.ua/companies/innovecs/",      // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://jobs.dou.ua/companies/ilogos/",        // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469
	"https://jobs.dou.ua/companies/natus-vincere/", // https://dou.ua/lenta/news/diia-city-has-been-officially-launched/#2343469

	"https://jobs.dou.ua/companies/megogonet-/", // https://dev.ua/news/megogo-vstupaeie-v-diia-sity

	"https://jobs.dou.ua/companies/roosh/",     // https://dou.ua/lenta/news/44-companies-applied-to-join-diia-city/
	"https://jobs.dou.ua/companies/softblues/", // https://dou.ua/lenta/interviews/nuzhnyi-about-trios/

	"https://jobs.dou.ua/companies/samsung/",                                                                  // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://jobs.dou.ua/companies/ria/",                                                                      // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://jobs.dou.ua/companies/rozetka-ua-internet-supermarket/",                                          // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://jobs.dou.ua/companies/plarium/",                                                                  // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
	"https://jobs.dou.ua/companies/privatne-aktsionerne-tovaristvo-tsentr-kompyuternih-tehnologij-infoplyus/", // https://dou.ua/lenta/news/55-first-residents-of-diia-city/
}
