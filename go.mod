module gitlab.com/YaroslavPodorvanov/StopDiiaCityVacancyStats

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/valyala/fasthttp v1.14.0
	gitlab.com/qrsx/dou-scrapper v0.0.0-20211003001111-f501649d45da
)
